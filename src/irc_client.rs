use crate::{chat_client::ChatClient, Result};
use anyhow::bail;
use futures::{future, future::Either, prelude::*};
use irc::client::{
    prelude::{Client, Config},
    ClientStream,
};
use irc_proto::Message;
use lockfree::channel::mpsc::Sender;
use std::sync::{atomic::AtomicBool, Arc, Mutex};

pub struct IrcClient {
    tx: Sender<crate::TaggedMessage>,
    config: Config,

    pub retry: Arc<Mutex<u8>>,

    stop: Arc<AtomicBool>,
}

/// Small wrapper for a read-only IRC connection
impl IrcClient {
    pub fn new(tx: Sender<crate::TaggedMessage>, config: Config, stop: Arc<AtomicBool>) -> Self {
        Self {
            tx,
            config,
            retry: Arc::new(Mutex::new(0)),
            stop,
        }
    }

    async fn next(&mut self, stream: &mut ClientStream) -> Result<Option<Message>> {
        let stopper = async move {
            loop {
                if self.stop.load(std::sync::atomic::Ordering::SeqCst) {
                    return true;
                }

                tokio::time::sleep(std::time::Duration::from_millis(100)).await;
            }
        };
        let next = stream.next();

        futures::pin_mut!(stopper);
        futures::pin_mut!(next);

        match future::select(stopper, next).await {
            Either::Left(_) => anyhow::bail!("Stopped"),
            Either::Right((f, _)) => f.transpose().map_err(Into::into),
        }
    }
}

#[async_trait::async_trait]
impl ChatClient for IrcClient {
    fn get_name(&self) -> &'static str {
        "IRC"
    }

    fn get_retry_count(&self) -> u8 {
        *self.retry.lock().unwrap()
    }

    fn get_retry(&mut self) -> Arc<Mutex<u8>> {
        self.retry.clone()
    }

    async fn connect(&mut self) -> crate::Result<()> {
        let mut client = Client::from_config(self.config.clone()).await?;

        // Queue sending of our user information
        client.identify()?;

        let tx = self.tx.clone();
        let retry = Arc::clone(&self.retry);

        let mut stream = client.stream()?;

        while let Some(message) = self.next(&mut stream).await? {
            if let Some(nickname) = message.source_nickname() {
                if client.current_nickname() == nickname {
                    match &message.command {
                        irc_proto::Command::PART(channel, _) => {
                            if self.config.channels.contains(&channel.to_lowercase()) {
                                // Somehow we left a configured channel, error out
                                bail!("Left configured channel {}", channel);
                            }
                        }
                        irc_proto::Command::JOIN(channel, _, _) => {
                            if self.config.channels.contains(&channel.to_lowercase()) {
                                log::info!("Joined {}", channel);

                                // Reset retry counter on successful join
                                *retry.lock().unwrap() = 0;
                            }
                        }
                        _ => (),
                    }
                }
            }

            // Forward all received messages
            tx.send(("[IRC]", message)).unwrap();
        }

        Ok(())
    }
}
