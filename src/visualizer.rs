use crate::Result;
use anyhow::anyhow;
use css_color::Rgba;
use cursive::{
    theme::{Color, ColorStyle},
    Cursive, Printer, Vec2, View,
};
use irc_proto::{prefix::Prefix, Command};
use lockfree::channel::*;
use parking_lot::{FairMutex as Mutex, RwLock};
use ringbuffer::{AllocRingBuffer, RingBufferExt, RingBufferWrite};
use std::sync::Arc;
use tasbot_display::{
    hardware::display::{DEFAULT_BRIGHTNESS, DEFAULT_GAMMA},
    tasbot::{NUM_PIXELS, PIXEL_POSITIONS, SCREEN_HEIGHT, SCREEN_WIDTH},
    RgbColor,
};

pub struct TasBotVisualizer {
    rx: Option<mpsc::Receiver<crate::TaggedMessage>>,
    cb_sink: cursive::CbSink,

    display: Arc<Mutex<Option<tasbot_display::Display>>>,
    front_buffer: Arc<Mutex<Vec<RgbColor>>>,
    back_buffer: Arc<Mutex<Vec<RgbColor>>>,
    message_history: Arc<RwLock<AllocRingBuffer<String>>>,
    num_pixels: usize,
    width: usize,
    height: usize,
}

impl TasBotVisualizer {
    pub fn new(rx: mpsc::Receiver<crate::TaggedMessage>, cb_sink: cursive::CbSink) -> Self {
        let display = tasbot_display::Display::new(NUM_PIXELS).ok();
        let front_buffer = vec![RgbColor::from([0, 0, 0]); NUM_PIXELS as usize];
        let back_buffer = vec![RgbColor::from([0, 0, 0]); NUM_PIXELS as usize];

        Self {
            rx: Some(rx),
            cb_sink,
            display: Arc::new(Mutex::new(display)),
            front_buffer: Arc::new(Mutex::new(front_buffer)),
            back_buffer: Arc::new(Mutex::new(back_buffer)),
            message_history: Arc::new(RwLock::new(AllocRingBuffer::with_capacity(256))),
            num_pixels: NUM_PIXELS as usize,
            width: SCREEN_WIDTH,
            height: SCREEN_HEIGHT,
        }
    }

    pub fn with_dimensions(
        rx: mpsc::Receiver<crate::TaggedMessage>,
        cb_sink: cursive::CbSink,
        width: usize,
        height: usize,
    ) -> Self {
        let num_pixels = width * height;
        let display = tasbot_display::Display::new(num_pixels as u32).ok();
        let front_buffer = vec![RgbColor::from([0, 0, 0]); width * height];
        let back_buffer = vec![RgbColor::from([0, 0, 0]); width * height];

        Self {
            rx: Some(rx),
            cb_sink,
            display: Arc::new(Mutex::new(display)),
            front_buffer: Arc::new(Mutex::new(front_buffer)),
            back_buffer: Arc::new(Mutex::new(back_buffer)),
            message_history: Arc::new(RwLock::new(AllocRingBuffer::with_capacity(256))),
            num_pixels,
            width,
            height,
        }
    }

    pub fn run(&mut self) {
        let display = Arc::clone(&self.display);
        let front_buffer = Arc::clone(&self.front_buffer);
        let back_buffer = Arc::clone(&self.back_buffer);
        let mut rx = self.rx.take().unwrap();
        let message_history = Arc::clone(&self.message_history);
        let cb_sink = self.cb_sink.clone();

        std::thread::spawn(move || loop {
            let mut updated = false;

            match rx.recv() {
                Ok((tag, message)) => {
                    if let Command::PRIVMSG(ref target, ref msg) = message.command {
                        let mut front_buffer = front_buffer.lock();

                        match Self::parse_message(
                            &mut front_buffer,
                            tag,
                            target,
                            message.prefix,
                            msg,
                            &message_history,
                        ) {
                            Ok(_) => (),
                            Err(err) => log::error!("{}", err),
                        }

                        updated = true;
                    }
                }
                Err(mpsc::NoSender) => break,
                Err(mpsc::NoMessage) => (),
            }

            if updated {
                let front_buffer = front_buffer.lock();
                Self::_update_display_buffer(&display, &front_buffer, &back_buffer);
                cb_sink.send(Box::new(Cursive::noop)).unwrap();
            }

            std::thread::sleep(std::time::Duration::from_millis(1000 / 15));
        });
    }

    fn _update_display_buffer(
        display: &Mutex<Option<tasbot_display::Display>>,
        front_buffer: &[RgbColor],
        back_buffer: &Mutex<Vec<RgbColor>>,
    ) {
        // Save a copy of the buffer for displaying
        back_buffer.lock().copy_from_slice(front_buffer);

        if let Some(display) = display.lock().as_mut() {
            // Commit the data to the display
            for (i, color) in front_buffer.iter().enumerate() {
                display[i] = *color;
            }

            display.draw();
        }
    }

    /// Push the front buffer to the display without re-drawing it
    pub fn update_display_buffer(&mut self) {
        let front_buffer = self.front_buffer.lock();
        Self::_update_display_buffer(&self.display, &front_buffer, &self.back_buffer);
    }

    /// Clear the current display buffer and re-draw it
    pub fn clear(&mut self) {
        self.message_history.write().clear();
        let mut front_buffer = self.front_buffer.lock();

        for pixel in front_buffer.iter_mut() {
            *pixel = RgbColor::from([0, 0, 0]);
        }

        Self::_update_display_buffer(&self.display, &front_buffer, &self.back_buffer);
    }

    /// Signal the display to re-draw
    pub fn draw_display_buffer(&self) {
        if let Some(display) = self.display.lock().as_mut() {
            display.draw();
        }
    }

    pub fn set_brightness(&mut self, brightness: f32) {
        if let Some(display) = self.display.lock().as_mut() {
            display.set_brightness(brightness);
        }
    }

    pub fn get_brightness(&self) -> f32 {
        if let Some(display) = self.display.lock().as_ref() {
            display.brightness()
        } else {
            DEFAULT_BRIGHTNESS
        }
    }

    pub fn set_gamma(&mut self, gamma: f32) {
        if let Some(display) = self.display.lock().as_mut() {
            display.set_gamma(gamma);
        }
    }

    pub fn set_per_channel_gamma(&mut self, gamma: [f32; 3]) {
        if let Some(display) = self.display.lock().as_mut() {
            display.set_per_channel_gamma(gamma)
        }
    }

    pub fn get_gamma(&self) -> Vec<f32> {
        if let Some(display) = self.display.lock().as_ref() {
            display.gamma().to_vec()
        } else {
            DEFAULT_GAMMA.to_vec()
        }
    }

    pub fn num_pixels(&self) -> usize {
        self.num_pixels
    }

    pub fn get_message_history(&self) -> Arc<RwLock<AllocRingBuffer<String>>> {
        Arc::clone(&self.message_history)
    }

    pub fn is_connected(&self) -> bool {
        self.display.lock().is_some()
    }

    fn color(&self, buffer: &[RgbColor], index: usize) -> Color {
        let color = &buffer[index];

        Color::Rgb(color[0], color[1], color[2])
    }

    fn parse_message(
        buffer: &mut [RgbColor],
        tag: &str,
        target: &str,
        author: Option<Prefix>,
        msg: &str,
        history: &RwLock<AllocRingBuffer<String>>,
    ) -> Result<()> {
        let mut valid = false;
        let mut iter = msg.split_whitespace();

        while let Some(color) = iter.next() {
            let rgba = color
                .parse::<Rgba>()
                .map_err(|_| anyhow!("Failed to parse"))?;

            let r = (rgba.red * 255.0).round() as u8;
            let g = (rgba.green * 255.0).round() as u8;
            let b = (rgba.blue * 255.0).round() as u8;

            let color = RgbColor::from([r, g, b]);

            if let Some(pixels) = iter.next() {
                for pixel in pixels.split(',') {
                    if pixel.contains('-') {
                        // Range
                        let mut iter = pixel.split('-');
                        let start = iter.next().unwrap().parse::<u8>();
                        let end = iter.next().unwrap().parse::<u8>();

                        if let (Ok(start), Ok(end)) = (start, end) {
                            for pixel in buffer
                                .iter_mut()
                                .skip(start as usize)
                                .take((end - start + 1) as usize)
                            {
                                *pixel = color;
                            }
                            valid = true;
                        }
                    } else if let Ok(id) = pixel.parse::<u8>() {
                        buffer[id as usize] = color;
                        valid = true;
                    }
                }
            }
        }

        if valid {
            if let Some(Prefix::Nickname(nickname, ..)) = author {
                history
                    .write()
                    .push(format!("{} {}: <{}> {}", tag, target, nickname, msg));
            } else {
                history.write().push(format!("{} {}: {}", tag, target, msg));
            }
        }

        Ok(())
    }
}

impl View for TasBotVisualizer {
    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        if constraint.y >= self.height && constraint.x >= self.width * 2 {
            Vec2::new(self.width * 2, self.height)
        } else {
            Vec2::new(self.width, self.height / 2)
        }
    }

    fn draw(&self, p: &Printer<'_, '_>) {
        let back_buffer = self.back_buffer.lock();

        let width = p.size.x;
        let height = p.size.y;
        let compact = height < self.height / 2 || width < self.width * 2;

        if self.num_pixels() == NUM_PIXELS as usize {
            // We are TASBot!

            if compact {
                for (y, rows) in PIXEL_POSITIONS.chunks_exact(2).enumerate() {
                    for (x, (top, bottom)) in rows[0].iter().zip(&rows[1]).enumerate() {
                        let (s, fg, bg) = match (top, bottom) {
                            (None, None) => (" ", Color::TerminalDefault, Color::TerminalDefault),
                            (Some(top), None) => {
                                ("▀", self.color(&back_buffer, *top), Color::TerminalDefault)
                            }
                            (None, Some(bottom)) => (
                                "▄",
                                self.color(&back_buffer, *bottom),
                                Color::TerminalDefault,
                            ),
                            (Some(top), Some(bottom)) => (
                                "▀",
                                self.color(&back_buffer, *top),
                                self.color(&back_buffer, *bottom),
                            ),
                        };

                        let style = ColorStyle::new(fg, bg);

                        p.with_color(style, |printer| {
                            printer.print((x, y), s);
                        });
                    }
                }
            } else {
                for (y, row) in PIXEL_POSITIONS.iter().enumerate() {
                    for (x, index) in row.iter().enumerate() {
                        if x == 9 || x == 18 || ((x == 10 || x == 17) && (y == 3 || y == 4)) {
                            let top = y % 2 == if x == 9 || x == 18 { 0 } else { 1 };
                            let index = if top {
                                PIXEL_POSITIONS[y + 1][x]
                            } else {
                                *index
                            };

                            let s = if top { "▄▄" } else { "▀▀" };

                            let (s, color) = match index {
                                None => ("  ", Color::TerminalDefault),
                                Some(index) => (s, self.color(&back_buffer, index)),
                            };

                            let style = ColorStyle::new(color, Color::TerminalDefault);

                            p.with_color(style, |printer| {
                                printer.print((x * 2, y), s);
                            });

                            continue;
                        }

                        let (s, color) = match index {
                            None => ("  ", Color::TerminalDefault),
                            Some(index) => ("██", self.color(&back_buffer, *index)),
                        };

                        let style = ColorStyle::new(color, color);

                        p.with_color(style, |printer| {
                            printer.print((x * 2, y), s);
                        });
                    }
                }
            }
        } else if compact {
            for y in (0..self.height).step_by(2) {
                let row_index = y * self.width;

                for x in 0..self.width {
                    let index = row_index + x;
                    let fg = self.color(&back_buffer, index);
                    let bg = self.color(&back_buffer, index + width);
                    let style = ColorStyle::new(fg, bg);

                    p.with_color(style, |printer| {
                        printer.print((x, y / 2), "▀");
                    });
                }
            }
        } else {
            for y in 0..self.height {
                let row_index = y * self.width;

                for x in 0..self.width {
                    let index = row_index + x;
                    let color = self.color(&back_buffer, index);
                    let style = ColorStyle::new(color, color);

                    p.with_color(style, |printer| {
                        printer.print((x * 2, y), "██");
                    });
                }
            }
        }
    }
}
