use std::sync::{Arc, Mutex};

#[async_trait::async_trait]
pub trait ChatClient {
    fn get_name(&self) -> &'static str {
        "Chat Client"
    }

    /// Opens a new connection and blocks the thread until the connection gets closed.
    async fn connect(&mut self) -> crate::Result<()>;
    fn get_retry(&mut self) -> Arc<Mutex<u8>>;
    fn get_retry_count(&self) -> u8;

    /// Connect to the server and reconnect when the connection is interrupted.
    async fn run(&mut self) {
        loop {
            log::info!("[{}] Connecting …", self.get_name());

            match self.connect().await {
                Ok(_) => (),
                Err(e) => log::error!("{:#?}", e),
            }

            self.delay();
        }
    }

    /// Used to sleep inbetween connection attempts
    fn delay(&mut self) {
        let mutex = self.get_retry();
        let mut retry = mutex.lock().unwrap();
        *retry += 1;

        let wait_time = std::cmp::min(60, (*retry - 1) * 5) as u64;

        log::info!("[{}] Sleeping for {} seconds", self.get_name(), wait_time);
        std::thread::sleep(std::time::Duration::from_secs(wait_time));
    }
}
