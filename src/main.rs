use crate::{
    chat_client::ChatClient, message_history::MessageHistory, visualizer::TasBotVisualizer,
};
use cursive::{
    view::{Nameable, Resizable, Scrollable},
    views::{Button, Dialog, LinearLayout, PaddedView, Panel, SliderView, TextView},
    Cursive,
};
use irc_proto::Message;
use lockfree::channel::*;
use std::sync::{atomic::AtomicBool, Arc};
use tasbot_display::hardware::display::MAX_BRIGHTNESS;

// TODO: Config
const CHANNELS: &[&str] = &["#dwangoac"];

pub mod chat_client;
pub mod message_history;
pub mod visualizer;

#[cfg(feature = "irc")]
pub mod irc_client;

#[cfg(feature = "parity-ws")]
pub mod twitch_ws_client;

pub type TaggedMessage = (&'static str, Message);
pub type Result<T> = anyhow::Result<T>;

#[cfg(feature = "parity-ws")]
async fn join_websocket(
    channels: &[&str],
    tx: mpsc::Sender<crate::TaggedMessage>,
    stop: Arc<AtomicBool>,
) {
    let mut client = twitch_ws_client::TwitchWsClient::new(
        tx,
        stop,
        &channels.iter().map(ToString::to_string).collect::<Vec<_>>(),
        &None,
        &None,
    );
    client.run().await
}

#[cfg(feature = "irc")]
async fn join_irc(
    channels: &[&str],
    tx: mpsc::Sender<crate::TaggedMessage>,
    stop: Arc<AtomicBool>,
) {
    // Generate a Twitch Chat guest name
    let username = format!(
        "justinfan{}",
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs()
    );

    // Fixed to Twitch Chat IRC for now
    let config = irc::client::prelude::Config {
        channels: channels.iter().map(ToString::to_string).collect::<Vec<_>>(),
        username: Some(username.clone()),
        nickname: Some(username),
        password: Some("justinfan".to_owned()),
        server: Some("irc.chat.twitch.tv".to_owned()),
        port: Some(6697),
        use_tls: Some(true),

        ..Default::default()
    };

    let mut client = irc_client::IrcClient::new(tx, config, stop);
    client.run().await
}

#[tokio::main]
async fn main() -> Result<()> {
    cursive::logger::init();
    log::set_max_level(log::LevelFilter::Info);

    let (tx, rx) = mpsc::create();
    let mut siv = cursive::default();
    let cb_sink = siv.cb_sink();

    let stop = Arc::new(AtomicBool::new(false));
    let mut display = TasBotVisualizer::new(rx, cb_sink.clone());
    // let mut display = TasBotVisualizer::with_dimensions(rx, 50, 20);

    let message_history = display.get_message_history();
    display.run();

    #[allow(clippy::redundant_clone)]
    #[cfg(feature = "parity-ws")]
    tokio::spawn(join_websocket(CHANNELS, tx.clone(), Arc::clone(&stop)));

    #[allow(clippy::redundant_clone)]
    #[cfg(feature = "irc")]
    tokio::spawn(join_irc(CHANNELS, tx.clone(), Arc::clone(&stop)));

    let mut buttons = LinearLayout::vertical()
        .child(PaddedView::lrtb(
            1,
            1,
            1,
            1,
            Button::new("Quit", |s| s.quit()),
        ))
        .child(PaddedView::lrtb(
            1,
            1,
            0,
            1,
            Button::new("Clear", |s| {
                s.call_on_name("display", |display: &mut TasBotVisualizer| {
                    display.clear();
                });
            }),
        ));

    if display.is_connected() {
        buttons.add_child(PaddedView::lrtb(
            1,
            1,
            0,
            1,
            Button::new("Set Brightness", |s| {
                const BRIGHTNESS_STEPS: f32 = 30.0;

                let brightness = s
                    .call_on_name("display", |display: &mut TasBotVisualizer| {
                        (display.get_brightness().min(MAX_BRIGHTNESS)
                            * (BRIGHTNESS_STEPS / MAX_BRIGHTNESS))
                            .round() as usize
                    })
                    .unwrap();

                s.add_layer(
                    Dialog::around(
                        SliderView::horizontal(BRIGHTNESS_STEPS as usize + 1)
                            .value(brightness)
                            .on_change(|s, n| {
                                let brightness = n as f32 / BRIGHTNESS_STEPS * MAX_BRIGHTNESS;

                                s.call_on_name("display", |display: &mut TasBotVisualizer| {
                                    display.set_brightness(brightness);
                                    display.draw_display_buffer();
                                });

                                s.call_on_name("brightness_dialog", |dialog: &mut Dialog| {
                                    dialog.set_title(&format!(
                                        "Brightness ({:>3.0} %)",
                                        brightness / MAX_BRIGHTNESS * 100.0
                                    ));
                                });
                            }),
                    )
                    .title(&format!(
                        "Brightness ({:>3.0} %)",
                        brightness as f32 / BRIGHTNESS_STEPS * 100.0
                    ))
                    .button("Ok", |s| {
                        s.pop_layer();
                    })
                    .with_name("brightness_dialog"),
                )
            }),
        ));

        buttons.add_child(PaddedView::lrtb(
            1,
            1,
            0,
            1,
            Button::new("Set Gamma", |s| {
                const GAMMA_STEPS: f32 = 30.0;
                const MIN_GAMMA: f32 = 1.0;
                const MAX_GAMMA: f32 = 3.0;

                fn get_title(c: &str, value: f32) -> String {
                    format!("{}{:>3$}({:0.1}): ", c, " ", value, 6 - c.chars().count())
                }

                let gamma: Vec<usize> = s
                    .call_on_name("display", |display: &mut TasBotVisualizer| {
                        display
                            .get_gamma()
                            .iter()
                            .map(|v| {
                                ((v - MIN_GAMMA).min(MAX_GAMMA) * (GAMMA_STEPS / MAX_GAMMA)).round()
                                    as usize
                            })
                            .collect()
                    })
                    .unwrap();

                let mut layout = LinearLayout::vertical();

                for (i, channel) in ["red", "green", "blue"].iter().enumerate() {
                    let name = {
                        let mut chars = channel.chars();
                        let first = chars.next().unwrap().to_ascii_uppercase();
                        let rest = chars.collect::<String>();
                        format!("{}{}", first, rest)
                    };
                    let name2 = name.clone();
                    let slider = SliderView::horizontal(GAMMA_STEPS as usize + 1)
                        .value(gamma[i])
                        .on_change(move |s, n| {
                            let gamma = s
                                .call_on_name("display", |display: &mut TasBotVisualizer| {
                                    display.get_gamma()
                                })
                                .unwrap();
                            let channel_gamma = n as f32 / GAMMA_STEPS * MAX_GAMMA + MIN_GAMMA;
                            let title = get_title(&name2, channel_gamma);

                            s.call_on_name(&format!("gamma_{}", channel), |view: &mut TextView| {
                                view.set_content(title)
                            });

                            s.call_on_name("display", |display: &mut TasBotVisualizer| {
                                match *channel {
                                    "red" => display.set_per_channel_gamma([
                                        channel_gamma,
                                        gamma[1],
                                        gamma[2],
                                    ]),
                                    "green" => display.set_per_channel_gamma([
                                        gamma[0],
                                        channel_gamma,
                                        gamma[2],
                                    ]),
                                    "blue" => display.set_per_channel_gamma([
                                        gamma[0],
                                        gamma[1],
                                        channel_gamma,
                                    ]),
                                    _ => unreachable!(),
                                }

                                display.draw_display_buffer();
                            });
                        });

                    let title =
                        get_title(&name, gamma[i] as f32 / GAMMA_STEPS * MAX_GAMMA + MIN_GAMMA);
                    let slider = LinearLayout::horizontal()
                        .child(TextView::new(&title).with_name(&format!("gamma_{}", channel)))
                        .child(slider);

                    layout.add_child(slider);
                }

                s.add_layer(Dialog::around(layout).title("Gamma").button("Ok", |s| {
                    s.pop_layer();
                }))
            }),
        ));
    }

    let history = Panel::new(
        MessageHistory::new(message_history)
            .scrollable()
            .scroll_x(true),
    )
    .title("Message History");

    let title = if display.is_connected() {
        "Visualizer"
    } else {
        "Visualizer (dc)"
    };

    let visualizer = Panel::new(display.with_name("display")).title(title);

    let layout = LinearLayout::vertical()
        .child(LinearLayout::horizontal().child(visualizer).child(buttons))
        .child(history);

    siv.add_global_callback('~', Cursive::toggle_debug_console);
    siv.add_global_callback('q', |s| s.quit());
    siv.add_fullscreen_layer(layout.full_screen());
    siv.run();

    stop.store(true, std::sync::atomic::Ordering::SeqCst);

    Ok(())
}
